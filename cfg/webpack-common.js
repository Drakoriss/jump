const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const location = require('./location');

module.exports = {
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: location.entryHtml
    })
  ],
  resolve: {
    extensions: ['.js'],
    modules: ['node_modules']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(location.sources),
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.(png|svg|jpg)$/,
        use: ['file-loader']
      }
    ]
  }
};
