const path = require('path');
const fs = require('fs');

const rootDir = fs.realpathSync(process.cwd());
const resolveDir = relativePath => path.resolve(rootDir, relativePath);

module.exports = {
  sources: resolveDir('src'),
  assets: resolveDir('src/assets'),
  dist: resolveDir('dist'),
  entryJs: resolveDir('src/index.js'),
  entryHtml: resolveDir('src/index.html')
};
