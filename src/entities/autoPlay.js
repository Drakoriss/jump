import { Texture, Sprite } from 'pixi.js';

let active = false;
let autoPlayButton;

function createAutoPlayButton(container, onTap, texture) {
  autoPlayButton = new Sprite(texture);
  autoPlayButton.scale.set(0.5);
  autoPlayButton.anchor.set(0.5);
  autoPlayButton.position.set(autoPlayButton.width >> 1);
  autoPlayButton.interactive = true;
  autoPlayButton.on('pointerdown', () => {
    active = !active;
    autoPlayButton.rotation = active ? Math.PI : 0;
    onTap();
  });
  container.addChild(autoPlayButton);
}

function deactivateAutoPlay() {
  active = false;
  autoPlayButton.rotation = 0;
}

export { createAutoPlayButton, deactivateAutoPlay };
