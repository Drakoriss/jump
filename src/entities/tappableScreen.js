import { Sprite, Texture, Rectangle } from 'pixi.js';
import { TYPES } from '../types';

function createTappableScreen(container, onTap) {
  const screen = new Sprite(Texture.EMPTY);
  screen.hitArea = new Rectangle(0, 0, TYPES.width, TYPES.height);
  screen.interactive = true;
  screen.on('pointerdown', onTap);
  container.addChild(screen);

  return screen;
}

export { createTappableScreen };
