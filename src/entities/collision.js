let safeCollisions = [];
let unsafeCollisions = [];

const COLLISION = {
  none: 'none',
  safe: 'safe',
  unsafe: 'unsafe',
};

function addSafeCollision(...args) {
  safeCollisions = [...safeCollisions, ...args];
}

function addUnsafeCollision(...args) {
  unsafeCollisions = [...unsafeCollisions, ...args];
}

function clear() {
  unsafeCollisions = [];
  safeCollisions = [];
}

function check(player) {
  const box = player.getBounds();
  const pX = box.x + 20;
  const pY = box.y;
  const prX = pX + box.width - 20;
  const pbY = pY + box.height - 20;

  for (const col of unsafeCollisions) {
    const bounds = col.getBounds();
    const { x } = bounds;
    const y = bounds.y + 20;
    const rX = x + bounds.width;
    const bY = y + bounds.height;

    if (isBetween(pX, x, x + 5) || isBetween(prX, x, x + 5)) {
      if (isBetween(pY, y, bY) || isBetween(pbY, y, bY)) {
        return COLLISION.unsafe;
      }
    }

    if (isBetween(x, pX, prX) || isBetween(x + 5, pX, prX)) {
      if (isBetween(y, pY, pbY) || isBetween(bY, pY, pbY)) {
        return COLLISION.unsafe;
      }
    }

    if (isBetween(pX, rX, rX - 5) || isBetween(prX, rX, rX - 5)) {
      if (isBetween(pY, y, bY) || isBetween(pbY, y, bY)) {
        return COLLISION.unsafe;
      }
    }

    if (isBetween(rX, pX, prX) || isBetween(rX - 5, pX, prX)) {
      if (isBetween(y, pY, pbY) || isBetween(bY, pY, pbY)) {
        return COLLISION.unsafe;
      }
    }
  }

  for (const col of safeCollisions) {
    const bounds = col.getBounds();
    const { x } = bounds;
    const { y } = bounds;
    const rX = x + bounds.width;

    if (isBetween(pX, x, rX) || isBetween(prX, x, rX)) {
      if (isBetween(pY, y, y + 5) || isBetween(pbY, y, y + 5)) {
        return COLLISION.safe;
      }
    }

    if (isBetween(x, pX, prX) || isBetween(rX, pX, prX)) {
      if (isBetween(y, pY, pbY) || isBetween(y + 5, pY, pbY)) {
        return COLLISION.safe;
      }
    }
  }

  return COLLISION.none;
}

// Checks if value is between a and b (inclusive)
function isBetween(value, a, b) {
  return value >= a && value <= b;
}

export { COLLISION, addSafeCollision, addUnsafeCollision, clear, check };
