import { Sprite } from 'pixi.js';
import { ease } from 'pixi-ease';
import { TYPES, SIDE } from '../types';
import { rand, pickFromArray } from '../util';

const pool = [];
const limit = Math.floor(TYPES.height / TYPES.rock.height) * 2;
let poolIndex = -1;

function sendRock(container, textures, side, onEnd) {
  let rock;

  if (pool.length >= limit) {
    rock = pickFromPool();
  } else {
    rock = createNew(textures);
    pool.push(rock);
  }
  rock.position.x =
    side === SIDE.left ? -TYPES.rock.width : TYPES.width + TYPES.rock.width;
  rock.position.y = (TYPES.height * 0.75) >> 0;
  container.addChild(rock);
  const anim = ease.add(
    rock,
    { x: TYPES.center.x },
    { ease: 'easeOutCubic', duration: 3000, wait: rand(0, 500) },
  );
  anim.once('complete', onEnd);

  return rock;
}

function pickFromPool() {
  poolIndex += 1;

  if (poolIndex >= limit) {
    poolIndex = 0;
  }

  return pool[poolIndex];
}

function createNew(textures) {
  const sprite = new Sprite(pickFromArray(textures));
  sprite.anchor.set(0.5);
  return sprite;
}

export { sendRock };
