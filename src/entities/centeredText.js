import { Text } from 'pixi.js';
import { TYPES } from '../types';

const centeredText = new Text('loading ...', {
  fill: TYPES.colors.text,
  dropShadow: true,
  dropShadowColor: TYPES.colors.textShadow,
  align: 'center',
});
centeredText.anchor.set(0.5);
centeredText.position.set(TYPES.center.x, TYPES.center.y);

export { centeredText };
