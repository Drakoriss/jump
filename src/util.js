function rand(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function pickFromArray(arr) {
  return arr[rand(0, arr.length - 1)];
}

export { rand, pickFromArray };
