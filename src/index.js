import {
  Application,
  AnimatedSprite,
  Container,
  Texture,
  Sprite,
  Spritesheet,
} from 'pixi.js';
import { ease } from 'pixi-ease';
import { TYPES, SIDE } from './types';
import { STATE, flow } from './state';
import atlas from './assets/GameAtlas.json';
import atlasImage from './assets/GameAtlas.png';
import { centeredText } from './entities/centeredText';
import { createTappableScreen } from './entities/tappableScreen';
import { sendRock } from './entities/rock';
import * as collision from './entities/collision';
import { createAutoPlayButton, deactivateAutoPlay } from './entities/autoPlay';

const app = new Application({
  width: TYPES.width,
  height: TYPES.height,
  ...TYPES.pixiConfig,
});
app.renderer.backgroundColor = TYPES.colors.bg;
document.querySelector('#root').appendChild(app.view);

app.stage.addChild(centeredText);

app.loader.add('atlas', atlasImage).load(onAssetsLoaded);

const rockTextures = [];
let side = SIDE.left;
const rocks = new Container();
const currentRock = new Container();
let currentHeight = (TYPES.height * 0.75) >> 0;
let autoPlay = false;
let player;

function onAssetsLoaded(_, resources) {
  const sheet = new Spritesheet(resources.atlas.texture.baseTexture, atlas);
  sheet.parse(() => {
    flow.nextState();
    for (let i = 1; i <= TYPES.blocksCount; i++) {
      rockTextures.push(Texture.from(`block_${i}`));
    }

    const playerTextures = [];
    for (let i = 1; i <= TYPES.playerCount; i++) {
      playerTextures.push(Texture.from(`hero_jump_${i}`));
    }

    centeredText.text = 'Tap!';
    const base = new Sprite(Texture.from('platform_01'));
    base.anchor.x = 0.5;
    base.position.set(TYPES.center.x, currentHeight);
    rocks.addChild(base);
    player = new AnimatedSprite(playerTextures);
    player.anchor.set(0.5);
    player.position.set(TYPES.center.x, currentHeight);
    player.loop = false;
    player.animationSpeed = 0.2;
    rocks.addChild(player);

    app.stage.addChild(rocks);
    app.stage.addChild(currentRock);
    createTappableScreen(app.stage, handleTap);
    createAutoPlayButton(
      app.stage,
      handleAutoPlayTap,
      Texture.from('auto_btn'),
    );
  });
}

let jump = false;
let jumpAnimation;
let isGameOver = false;

function jumpAction() {
  if (flow.getState() === STATE.ongoing) {
    if (!jump) {
      jump = true;
      jumpAnimation = ease.add(
        player,
        { y: player.y - (TYPES.rock.height << 1) },
        { ease: 'easeOutQuad', duraton: 500, reverse: true },
      );
      player.gotoAndPlay(0);
      jumpAnimation.once('complete', () => {
        jump = false;
      });
    }
  }
}

function startAction() {
  if (flow.getState() === STATE.pause) {
    centeredText.visible = false;
    flow.nextState();
    app.ticker.add(sendRocks);
    app.ticker.add(collisionCheck);
    sendNext = true;
  }
}

function handleTap() {
  if (isGameOver || autoPlay) {
    return;
  }
  const state = flow.getState();
  if (state === STATE.pause) {
    startAction();
    return;
  }
  if (state === STATE.ongoing) {
    jumpAction();
    return;
  }
  if (state === STATE.gameover) {
    centeredText.text = 'Tap!';
    rocks.scale.set(1);
    if (rocks.children.length > 2) {
      const children = rocks.removeChildren(2);
      for (const c of children) {
        c.x = -TYPES.rock.width;
      }
    }
    rocks.x = 0;
    rocks.y = 0;
    const children = currentRock.removeChildren();
    for (const c of children) {
      c.x = -TYPES.rock.width;
      c.alpha = 1;
    }
    currentHeight = (TYPES.height * 0.75) >> 0;
    player.position.set(TYPES.center.x, currentHeight);
    player.skew.set(0);
    player.rotation = 0;
    player.visible = true;
    flow.nextState();
  }
}

function handleAutoPlayTap() {
  const state = flow.getState();
  if (state === STATE.ongoing || state === STATE.pause) {
    autoPlay = !autoPlay;
    if (state === STATE.pause) {
      startAction();
    } else {
      jumpAction();
    }
  }
}

let sendNext = true;

function handleFlyEnd() {
  collision.clear();
  side = side === SIDE.left ? SIDE.right : SIDE.left;
  const current = currentRock.children[0];
  rocks.addChild(current);
  current.y = currentHeight;
  currentHeight -= TYPES.rock.height;
  ease.add(
    rocks,
    { y: rocks.y + TYPES.rock.height },
    { ease: 'easeInOutQuad' },
  );
  sendNext = true;
}

function sendRocks() {
  if (flow.getState() === STATE.ongoing) {
    if (sendNext) {
      sendNext = false;
      const current = sendRock(currentRock, rockTextures, side, handleFlyEnd);
      collision.addSafeCollision(current);
      collision.addUnsafeCollision(current);
      if (autoPlay) {
        setTimeout(() => jumpAction(), 500);
      }
    }
  }
}

function collisionCheck() {
  if (flow.getState() === STATE.ongoing) {
    const hit = collision.check(player);
    if (hit === collision.COLLISION.unsafe) {
      gameOver();
      return;
    }
    if (hit === collision.COLLISION.safe) {
      const current = currentRock.children[0];
      if (current) {
        setTimeout(() => ease.removeEase(current), 200);
      }
      ease.removeEase(player);
      handleFlyEnd();
      jump = false;
    }
  }
}

function gameOver() {
  isGameOver = true;
  autoPlay = false;
  deactivateAutoPlay();
  flow.nextState();
  ease.removeEase(player);
  const current = currentRock.children[0];

  if (current) {
    ease.removeEase(current);
    ease.add(current, { alpha: 0 });
  }

  jump = false;
  sendNext = false;
  app.ticker.remove(sendRocks);
  app.ticker.remove(collisionCheck);
  centeredText.text = 'GAME OVER\ntap to reset';
  centeredText.visible = true;
  const x = side === SIDE.left ? TYPES.width : 0;
  const anim = ease.add(player, {
    rotation: Math.PI << 1,
    skew: Math.PI,
    position: { x, y: player.y + 600 },
  });
  anim.once('complete', () => {
    player.visible = false;
    const towerAnim = ease.add(rocks, {
      scale: 0.5,
      x: TYPES.center.x >> 1,
      y: TYPES.center.y,
    });
    towerAnim.once('complete', () => {
      isGameOver = false;
    });
  });
}
