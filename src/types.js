const WIDTH = 720;
const HEIGHT = 1280;

const TYPES = {
  width: WIDTH,
  height: HEIGHT,
  center: {
    x: WIDTH >> 1,
    y: HEIGHT >> 1,
  },
  rock: {
    width: 180,
    height: 84,
  },
  player: {
    width: 140,
    height: 140,
  },
  pixiConfig: {
    antialias: true,
  },
  colors: {
    bg: 0x272822,
    text: 0xffffff,
    textShadow: 0x000000,
  },
  blocksCount: 11,
  playerCount: 10,
};

const SIDE = {
  left: 'left',
  right: 'right',
};

export { TYPES, SIDE };
