const STATE = {
  loading: 'loading',
  pause: 'pause',
  ongoing: 'ongoing',
  gameover: 'gameover',
};

const stateFlow = [STATE.pause, STATE.ongoing, STATE.gameover];

let currentState = STATE.loading;
let stateIndex = -1;

function getState() {
  return currentState;
}

function nextState() {
  stateIndex += 1;

  if (stateIndex >= stateFlow.length) {
    stateIndex = 0;
  }

  currentState = stateFlow[stateIndex];

  return currentState;
}

const flow = { getState, nextState };

export { STATE, flow };
