# Jump

Game about jumping

## Start
`yarn` and `yarn start:prod` to start production version of a game then proceed to `localhost:5000`

## Production bundle
If you need a fresh production bundle just `yarn build` and look inside a `dist` folder

## Development
`yarn` and `yarn start` to start dev server with HMR. Then proceed to `localhost:3000`


